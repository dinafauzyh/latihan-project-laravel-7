<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::group(['middleware' => ['auth']], function () {
    //CRUD Genre
    Route::resource('genre', 'GenreController');

    //CRUD Cast
    Route::resource('cast', 'CastController');

    //CRUD Role
    Route::resource('role', 'RoleController');
});

//CRUD Film
Route::resource('film', 'FilmController');

Route::resource('profile', 'ProfileController')->only([
    'index', 'update', 'edit'
]);

Auth::routes();
