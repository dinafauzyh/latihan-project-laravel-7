@extends('layout.master')
@section('title')
    Detail Cast {{ $cast->nama }}
@endsection

@section('content')
<h2 class="text-primary">{{$cast->nama}}</h2>
<p>Umur {{$cast->umur}} tahun</p>
<p>{{$cast->bio}}</p>
@endsection