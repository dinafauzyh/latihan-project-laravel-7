<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    protected $fillable = ['nama', 'cast_id', 'film_id'];

    public function cast(){
        return $this->belongsTo('App\Cast');
    }

    public function film(){
        return $this->belongsTo('App\Film');
    }

}
