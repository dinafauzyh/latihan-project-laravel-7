@extends('layout.master')
@section('title')
    Edit Data Role
@endsection

@push('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('template/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endpush

@push('script')
    <script src="{{asset('template/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        
            //Initialize Select2 Elements
            $('.select2bs4').select2({
              theme: 'bootstrap4'
            })
        })
    </script>
@endpush

@section('content')
    
    <div>
        <form action="/role/{{$role->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama Role</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{$role->nama}}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="film_id">Film</label>
                <select name="film_id" id="film_id" class="form-control select2" style="width: 100%;">
                    <option value="">Pilih Film</option>
                    @foreach ($film as $item)
                        @if ($item->id == $role->film_id)
                            <option value="{{$item->id}}" selected>{{$item->judul}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->judul}}</option>
                        @endif                        
                    @endforeach
                </select>
                @error('film_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="cast_id">Cast</label>
                <select name="cast_id" id="cast_id" class="form-control select2" style="width: 100%;">
                    <option value="">Pilih Cast</option>
                    @foreach ($cast as $item)
                        @if ($item->id == $role->cast_id)
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endif   
                    @endforeach
                </select>
                @error('cast_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection