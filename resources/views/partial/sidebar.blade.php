<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->

      @auth
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="{{asset('template/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="/profile" class="d-block">{{ Auth::user()->name }} ({{ Auth::user()->profile->umur}})</a>
          </div>
        </div>
      @endauth

      @guest
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="{{asset('template/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">Guest / Belum Login</a>
          </div>
        </div>
      @endguest

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @auth
            <li class="nav-item">
              <a href="/dashboard" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/genre" class="nav-link">
                <i class="nav-icon far fa fa-ticket-alt"></i>
                <p>
                  Genre
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/cast" class="nav-link">
                <i class="nav-icon far fa fa-users"></i>
                <p>
                  Cast
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/role" class="nav-link">
                <i class="nav-icon far fa fa-user-circle"></i>
                <p>
                  Role
                </p>
              </a>
            </li>
          @endauth

          <li class="nav-item">
            <a href="/film" class="nav-link">
              <i class="nav-icon far fa fa-film"></i>
              <p>
                Film
              </p>
            </a>
          </li>

          @guest
            <li class="nav-item">
              <a href="/login" class="nav-link">
                <i class="nav-icon far fa fa-sign-in-alt"></i>
                <p>
                  Login
                </p>
              </a>
            </li>
          @endguest

          @auth           
            <li class="nav-item">
              <a href="/profile" class="nav-link">
                <i class="nav-icon far fa fa-cog"></i>
                <p>
                  Profile
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                  <i class="nav-icon far fa fa-sign-out-alt"></i>
                  <p>
                    Logout
                  </p>
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
              </form>
            </li>
          @endauth

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>