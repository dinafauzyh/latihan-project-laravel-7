@extends('layout.master')
@section('title')
    Data Film
@endsection


@section('content')
<div class="card">
    @auth
        <div class="card-header">
            <h3 class="card-title"><a href="/film/create" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</a>
            </h3>
        </div> 
    @endauth
    <div class="row"> 
        @foreach ($film as $item)
        <div class="col-4">
            <div class="card mt-3 ml-4" style="width: 18rem;">
                <img class="card-img-top" src="{{asset('img/'.$item->poster)}}" alt="Film Poster">
                <div class="card-body">
                    <h5 class="card-title"><strong>{{$item->judul}} ({{$item->tahun}})</strong></h5>
                    <p class="card-text"><span class="badge badge-primary">{{$item->genre->nama}}</span></p>
                    <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>

                    @auth
                    <form action="/film/{{$item->id}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <a href="/film/{{$item->id}}" class="btn btn-info">Read More</a>
                        <a href="/film/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                    @endauth

                    @guest
                        <a href="/film/{{$item->id}}" class="btn btn-info">Read More</a>
                    @endguest
                {{--  <small>{{$item->created_at}}</small>  --}}
                </div>
            </div> 
        </div> 
        @endforeach
    </div>
</div>
@endsection