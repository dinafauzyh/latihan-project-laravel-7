@extends('layout.master')
@section('title')
    Edit Data Genre
@endsection

@section('content')
    
    <div>
        <form action="/genre/{{$genre->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Genre</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{$genre->nama}}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection