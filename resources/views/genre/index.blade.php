@extends('layout.master')
@section('title')
    Genre
@endsection

@push('script')
  <script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush

@push('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.2/datatables.min.css"/>
@endpush

@push('script')
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endpush

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title"><a href="/genre/create" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</a>
      </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>Genre</th>
          <th>List Film</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($genre as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>
                  <ul>
                    @foreach ($value->film as $item)
                        <li>{{$item->judul}}</li>
                    @endforeach
                  </ul>
                </td>
                <td>
                    <form action="/genre/{{$value->id}}" method="POST">
                        @csrf
                        <a href="/genre/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    </form>

                      <a href="#" data-id="{{$value->id}}" class="btn btn-danger my-1 swal-confirm">
                        <form action="/genre/{{$value->id}}" id="delete{{$value->id}}" method="POST">
                          @csrf
                          @method('DELETE')
                        </form>
                        Delete
                      </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">No data</td>
            </tr>  
        @endforelse   
        </tbody>
        <tfoot>
      </table>
    </div>
    <!-- /.card-body -->

</div>
@endsection

@push('script')
  <script>
    $(".swal-confirm").click(function(e){
      id = e.target.dataset.id;
      swal({
        title: 'Yakin akan hapus data?',
        text: 'Data yang sudah dihapus tidak bisa dikembalikan!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
          if(willDelete){
            swal('Data Berhasil Dihapus!', {
              icon: 'success',
            });
            $(`#delete${id}`).submit();
          } else {
            //swal('Your imaginary file is safe!');
          }
      });
    });
  </script>
@endpush