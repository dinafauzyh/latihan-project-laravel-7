@extends('layout.master')
@section('title')
    Tambah Data Role
@endsection

@push('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('template/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endpush

@push('script')
    <script src="{{asset('template/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        
            //Initialize Select2 Elements
            $('.select2bs4').select2({
              theme: 'bootstrap4'
            })
        })
    </script>
@endpush

@section('content')
    
    <div>
        <form action="/role" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Role</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Peran">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="film_id">Film</label>
                <select name="film_id" id="film_id" class="form-control select2" style="width: 100%;">
                    <option value="">Pilih Film</option>
                    @foreach ($film as $item)
                        <option value="{{$item->id}}">{{$item->judul}}</option>
                    @endforeach
                </select>
                @error('film_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="cast_id">Cast</label>
                <select name="cast_id" id="cast_id" class="form-control select2" style="width: 100%;">
                    <option value="">Pilih Cast</option>
                    @foreach ($cast as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
                @error('cast_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
@endsection