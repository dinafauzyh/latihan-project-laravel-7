@extends('layout.master')
@section('title')
    Data Cast
@endsection

@push('script')
  <script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.2/datatables.min.css"/>
@endpush

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title"><a href="/cast/create" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</a>
      </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>Nama</th>
          <th>List Film</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>
                  <ul>
                    @foreach ($value->role as $item)
                        <li>{{$item->film->judul}}</li>
                    @endforeach
                  </ul>
                </td>
                <td>
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4">No data</td>
            </tr>  
        @endforelse   
        </tbody>
        <tfoot>
      </table>
    </div>
    <!-- /.card-body -->

</div>
@endsection