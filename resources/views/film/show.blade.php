@extends('layout.master')
@section('title')
    Detail Film {{ $film->judul }}
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col">
                <img src="{{asset('img/'.$film->poster)}}" alt="Film Poster" class="mt-2">
            </div>
            <div class="col-5">
                <h4 class="text-info">Cast</h4>
                <ul class="text-justify">
                    @foreach ($film->role as $item)
                        <li>{{$item->cast->nama}} - {{$item->nama}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        <h2 class="text-primary pt-3">{{$film->judul}} ({{$film->tahun}})</h2>
        <p class="text-justify">{{$film->ringkasan}}</p>
        <a href="/film" class="btn btn-secondary"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Kembali</a>
    </div>
</div>
@endsection

{{--  <div class="card mt-3 ml-4" style="width: 18rem;">
    <img class="card-img-top" src="{{asset('img/'.$film->poster)}}" alt="Film Image">
    <div class="card-body">
    <h5 class="card-title"><strong>{{$film->judul}} ({{$film->tahun}})</strong></h5>
    <p class="card-text">{{$film->ringkasan}}</p>
    <a href="/film" class="btn btn-secondary">Kembali</a>
    {{--  <small>{{$item->created_at}}</small>  --}}
    {{--  </div>
</div>  --}}