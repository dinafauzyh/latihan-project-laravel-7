<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Profile;
use Auth;

class ProfileController extends Controller
{
    
    public function index(){
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.index', compact('profile'));
    }

    public function edit($id){
        $profile = Profile::find($id);
        return view('profile.edit', compact('profile'));
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'umur' => 'required',
            'alamat' => 'required',
            'bio' => 'required'
        ]);

        $profile_data = [
            'umur' => $request->umur,
            'alamat' => $request->alamat,
            'bio' => $request->bio
        ];

        Profile::whereId($id)->update($profile_data);

        Alert::success('Berhasil', 'Profile Berhasil Diubah');
        return redirect('/profile');
    }
}
