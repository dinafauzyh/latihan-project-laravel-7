@extends('layout.master')
@section('title')
   Info Profile User
@endsection

@section('content')
<div class="">
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td><b>Nama:</b> <br> {{$profile->user->name}} </td>
                        <td><b>E-mail:</b> <br> {{$profile->user->email}} </td>

                    </tr>
                    <tr>
                        <td><b>Umur:</b> <br> {{$profile->umur}} </td>
                        <td><b>Alamat lengkap:</b><br> {{$profile->alamat}} </td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>Bio:</b> <br> {{$profile->bio}} </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <a href="/profile/{{$profile->id}}/edit" class="btn btn-primary">Edit Profile</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>

</div>
@endsection