# Project Details

Menu's:
<ol>
    <li>Dashboard</li>
    <li>Genre</li>
    <li>Cast</li>
    <li>Role</li>
    <li>Film</li>
    <li>Profile</li>
</ol>

To access <b>CRUD Genre, Cast, Role, dan Film</b> please <b>register</b> first.

If you're not logged in, you can only access <b>http://127.0.0.1:8000/film</b> page.

<br>

## Installation Guide
1. Clone this repository
```cmd
git clone https://gitlab.com/dinafauzyh/latihan-project-laravel-7.git

cd latihan-project-laravel-7
```

2. Update Composer
```cmd
composer update
```

3. Generate key
```cmd
php artisan key:generate
```

4. Migration
```cmd
php artisan migrate
```

5. Run Server
```cmd
php artisan serve
```

6. You can start using the application now, and add data to database.

<br>

## Plugin
<ol>
    <li>DataTable (CDN)</li>
    <li>Sweet Alert</li>
</ol>

```cmd
composer require realrashid/sweet-alert
```