<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Role;
use App\Cast;
use App\Film;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::all();
        return view('role.index', compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cast = Cast::all();
        $film = Film::all();
        return view('role.create', compact('cast', 'film'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'cast_id' => 'required',
            'film_id' => 'required'
        ]);

        Role::create([
            'nama' => $request->nama,
            'cast_id' => $request->cast_id,
            'film_id' => $request->film_id
        ]);

        Alert::success('Berhasil', 'Data Role Berhasil Ditambahkan');
        return redirect('/role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::all();
        $film = Film::all();
        $role = Role::find($id);
        return view('role.edit', compact('cast', 'film', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'cast_id' => 'required',
            'film_id' => 'required'
        ]);

        $role = Role::find($id);
        $role_data = [
            'nama' => $request->nama,
            'cast_id' => $request->cast_id,
            'film_id' => $request->film_id
        ];
        
        $role->update($role_data);
        Alert::success('Berhasil', 'Data Role Berhasil Diubah');
        return redirect('/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();

        return redirect('/role');
    }
}
