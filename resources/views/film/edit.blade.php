@extends('layout.master')
@section('title')
    Edit Data Film {{$film->judul}}
@endsection

@section('content')
    
<div>
        <form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">Judul Film</label>
                <input type="text" class="form-control" name="judul" id="judul" value="{{$film->judul}}">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tahun">Tahun Film</label>
                <input type="number" class="form-control" name="tahun" id="tahun" value="{{$film->tahun}}">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="ringkasan">Ringkasan</label>
                <textarea class="form-control" name="ringkasan" id="ringkasan" cols="30" rows="10">{{$film->ringkasan}}</textarea>
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="genre_id">Genre</label>
                <select name="genre_id" id="genre_id" class="form-control">
                    <option value="">----- Pilih Genre -----</option>
                    @foreach ($genre as $item)
                        @if ($item->id == $film->genre_id)
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endif
                    @endforeach
                </select>
                @error('genre_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="poster">Poster Film</label>
                <input type="file" class="form-control" name="poster" id="poster">
                @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
@endsection